
import sys
import csv
from Report import Report

# Assiment1 Completed.
rpt = Report()

if len(sys.argv) < 2:
    print("Error need path")
    theFile = '../2017.annual.singlefile.csv'
    fileFips = '../area_titles.csv'
    # theFile = '../smaller.csv'
else:
    theFile = sys.argv[1] + '/2017.annual.singlefile.csv'
    fileFips = sys.argv[1] + '/area_titles.csv'


def getName(row, fips):
    if (int(fips[1]) == int(row[0])):
        return [row[1], int(fips[0])] # returns name, wage

def findElement(list):
    return list[1]
def findElement2(list):
    return list[0]

def getUnique(myList):
    myDict = {}
    count = 0
    for i in range(len(myList)):     #how to get one key
        if myList[i] in myDict:
            myDict[myList[i]]+=1
        else:
            myDict[myList[i]] = 1
    for i in myDict.values():
        if i == 1:
            count+=1
    return count
def gettingTheName(annaulRank,estabRank,empLvlRank):
    rankingWage = []
    rankingEstab = []
    rankingEmpLvl = []

    with open(fileFips) as data:
        reader = csv.reader(data)
        for row in reader:
            for i in range(5):
                if(row[0] == annaulRank[i][1]):
                    rankingWage.append(getName(row, annaulRank[i]))
                    rankingWage = sorted(rankingWage, key = findElement, reverse=True)
                if(row[0]== estabRank[i][1]):
                    rankingEstab.append(getName(row,estabRank[i]))
                    rankingEstab = sorted(rankingEstab, key = findElement, reverse=True)
                if(row[0]== empLvlRank[i][1]):
                    rankingEmpLvl.append(getName(row,empLvlRank[i]))
                    rankingEmpLvl = sorted(rankingEmpLvl, key = findElement, reverse=True)
    return rankingWage, rankingEstab, rankingEmpLvl


def software(filename):
    with open(filename) as dataFile:
        reader = csv.reader(dataFile)
        global rpt
        hold2 = 0
        holdMax2 = 0
        numberOfFips2 = 0
        annualWage2 = 0
        numOfEstab2 = 0
        holdEstab2 = 0
        numOfEmpLvl2 = 0
        holdEmpLvl2 = 0
        annaualList = []
        estabList = []
        EmpLvlList = []
        annaulRank = []
        estabRank = []
        empLvlRank = []

        for row in reader:
            if(not row[0][0].isalpha() and not row[0].endswith("000")and row[2] == "5112" and row[1] == "5"):
                #     total Employment level.
                numOfEmpLvl2 = numOfEmpLvl2 + float(row[9])
                EmpLvlList.append(row[9])
                empLvlRank.append([int(row[9]), row[0]])
                if (float(row[9]) > float(holdEmpLvl2)):
                    holdEmpLvl2 = float(row[9])
                    maxEmpLvl2 = row[0]

                    # total estab number
                numOfEstab2 = numOfEstab2 + int(row[8])
                estabList.append(row[8])
                estabRank.append([int(row[8]), row[0]])
                if (float(row[8]) > float(holdEstab2)):
                    holdEstab2 = float(row[8])
                    maxEstab2 = row[0]
                    # Total annual Wage
                annualWage2 = annualWage2 + float(row[10])
                annaualList.append(row[10])
                # make 2d list
                annaulRank.append([int(row[10]),row[0]])

                if (float(row[10]) > float(holdMax2)):
                    holdMax2 = float(row[10])
                    maxArea2 = row[0]

                # count number of fips
                if (hold2 != row[0]):
                    numberOfFips2 = numberOfFips2 + 1
                hold2 = row[0]
        # make Lists into sets
        annaualSet = set(annaualList)
        estabSet = set(estabList)
        EmpLvlSet = set(EmpLvlList)

        annaualU = getUnique(annaualList)
        estabU = getUnique(estabList)
        empLvlU = getUnique(EmpLvlList)

        # annaulUnique = dict(annaualList)


        annaulRank = sorted(annaulRank, key=findElement2, reverse=True)
        estabRank = sorted(estabRank, key=findElement2, reverse=True)
        empLvlRank = sorted(empLvlRank, key=findElement2, reverse=True)  #WHY IS IT NOT SORTING RIGHT FOR SOFT??

        countA = 0
        countEstab = 0
        countEmp = 0
        annaulCash = 0
        estabCash = 0
        empLvlCash = 0
        cashCounty = 49005
        for i in annaulRank:
            if int(annaulRank[countA][1]) == cashCounty:
                annaulCash = countA + 1
            countA = countA + 1
        for i in estabRank:
            if int(estabRank[countEstab][1]) == cashCounty:     # This is not right. + 20 is how far im off
                estabCash = countEstab + 1
            countEstab = countEstab + 1
        for i in empLvlRank:
            if int(empLvlRank[countEmp][1]) == cashCounty:
                empLvlCash = countEmp + 1
            countEmp = countEmp + 1

        rankingWage, rankingEstab, rankingEmpLvl = gettingTheName(annaulRank, estabRank, empLvlRank)


        rpt.soft.distinct_pay = len(annaualSet)
        rpt.soft.distinct_estab = len(estabSet)
        rpt.soft.distinct_empl = len(EmpLvlSet)
        rpt.soft.per_capita_avg_wage = annualWage2/ numOfEmpLvl2
        rpt.soft.top_annual_wages = rankingWage
        rpt.soft.top_annual_avg_emplvl = rankingEmpLvl
        rpt.soft.top_annual_estab = rankingEstab

        rpt.soft.count= numberOfFips2
        rpt.soft.total_pay = (int(annualWage2))
        rpt.soft.total_estab = numOfEstab2
        rpt.soft.total_empl = int(numOfEmpLvl2)

        rpt.soft.unique_pay = annaualU#annaulUnique  # NEED TO FIND THE UNIQUE PAY! DICTIONARY
        rpt.soft.cache_co_pay_rank = annaulCash

        rpt.soft.unique_estab = estabU
        rpt.soft.cache_co_estab_rank = estabCash

        rpt.soft.unique_empl = empLvlU
        rpt.soft.cache_co_empl_rank = empLvlCash




def getData(filename):
    global rpt
    with open (filename) as dataFile:
        reader = csv.reader(dataFile)
        hold = 0
        holdMax = 0
        numberOfFips = 0
        annualWage = 0
        numOfEstab = 0
        holdEstab = 0
        numOfEmpLvl = 0
        holdEmpLvl = 0

        annaualList = []
        estabList = []
        EmpLvlList = []
        annaulRank = []
        estabRank = []
        empLvlRank = []

        for row in reader:
            if(not row[0][0].isalpha() and not row[0].endswith("000") and row[2] == "10" and row[1] == "0"):
                #     Total Employment level
                numOfEmpLvl = numOfEmpLvl + float(row[9])
                EmpLvlList.append(row[9])
                empLvlRank.append([int(row[9]), row[0]])
                if(float(row[9]) > float(holdEmpLvl)):
                    holdEmpLvl = float(row[9])
                    maxEmpLvl = row[0]

                    # total estab number
                numOfEstab = numOfEstab + int(row[8])
                estabList.append(row[8])

                estabRank.append([int(row[8]), row[0]])

                if(float (row[8]) > float (holdEstab)):
                    holdEstab = float(row[8])
                    maxEstab = row[0]
                    # Total annual Wage
                annualWage = annualWage + float(row[10])
                annaualList.append(row[10])
                # makes a 2d list
                fipsListWage = [int(row[10]), row[0]] # Fips Codes Annual Wage NOT WORKING!!!!
                annaulRank.append(fipsListWage)

                if(float (row[10]) > float (holdMax)):
                    holdMax = float(row[10])
                    maxArea = row[0]

                # count number of fips
                if(hold != row[0]):
                    numberOfFips = numberOfFips + 1
                hold = row[0]
        # make Lists into sets
        annaualSet = set(annaualList)
        estabSet = set(estabList)
        EmpLvlSet = set(EmpLvlList)

        annaualU = getUnique(annaualList)
        estabU = getUnique(estabList)
        empLvlU = getUnique(EmpLvlList)

        annaulRank = sorted(annaulRank, reverse=True)
        estabRank = sorted(estabRank, reverse=True)
        empLvlRank = sorted(empLvlRank, reverse=True)
        countA = 0
        countEstab = 0
        countEmp = 0
        annaulCash = 0
        estabCash = 0
        empLvlCash = 0
        cashCounty = 49005
        for i in annaulRank:

            if int(annaulRank[countA][1]) == cashCounty:
                annaulCash = countA + 1
            countA = countA + 1
        for i in estabRank:
            if int(estabRank[countEstab][1]) == cashCounty:
                estabCash = countEstab + 1
            countEstab = countEstab + 1
        for i in empLvlRank:
            if int(empLvlRank[countEmp][1]) == cashCounty:
                empLvlCash = countEmp + 1
            countEmp = countEmp + 1

        rankingWage, rankingEstab, rankingEmpLvl = gettingTheName(annaulRank,estabRank,empLvlRank)


    rpt.all.distinct_pay = len(annaualSet)              # Works
    rpt.all.distinct_estab = len(estabSet)              # Works
    rpt.all.distinct_empl = len(EmpLvlSet)              # Works
    rpt.all.per_capita_avg_wage = (annualWage / numOfEmpLvl)    # Works
    rpt.all.top_annual_wages = rankingWage              # Works
    rpt.all.top_annual_avg_emplvl = rankingEmpLvl       # Works
    rpt.all.top_annual_estab = rankingEstab             # Works

    rpt.all.count = numberOfFips                        # Works
    rpt.all.total_pay = (int(annualWage))               # Works
    rpt.all.total_estab = numOfEstab                    # Works
    rpt.all.total_empl = int(numOfEmpLvl)               # Works

    rpt.all.unique_pay = annaualU                             # NEED TO FIND THE UNIQUE PAY! DICTIONARY
    rpt.all.cache_co_pay_rank = annaulCash              # Works

    rpt.all.unique_estab = estabU
    rpt.all.cache_co_estab_rank = estabCash

    rpt.all.unique_empl = empLvlU
    rpt.all.cache_co_empl_rank = empLvlCash

    # These are lists of pairs (2-tuples): 0th element is a FIPS area name, 1th element is a numeric value



## TODO: Add your code here

# runs the all the code above
getData(theFile)
software(theFile)






# By the time you submit your work to me, this should be the *only* output your
# entire program produces.
print(rpt)
